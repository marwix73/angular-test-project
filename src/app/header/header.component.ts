import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  @ViewChild("headerMenu")
  menu: ElementRef<HTMLDivElement>;

  @ViewChild("header")
  header: ElementRef<HTMLDivElement>;

  constructor() {
  }

  ngOnInit(): void {
  }

  toggleHeaderMenu(event: any) {
    event.stopPropagation();
    this.menu.nativeElement.classList.toggle('active');
  }

  @HostListener('document:scroll', ['$event'])
  documentScroll() {
    if (window.scrollY > 0) {
      this.header.nativeElement.classList.add('colored');
      return;
    }

    this.header.nativeElement.classList.remove('colored');
  }

  @HostListener('document:click', ['$event'])
  documentClick() {
    this.menu.nativeElement.classList.remove('active');
  }
}
