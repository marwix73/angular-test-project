import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PointBannerComponent } from './point-banner.component';

describe('PointBannerComponent', () => {
  let component: PointBannerComponent;
  let fixture: ComponentFixture<PointBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PointBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PointBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
