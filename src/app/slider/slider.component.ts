import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})

export class SliderComponent implements OnInit {
  sliderConfig = {
    "slidesToShow": 1,
    "slidesToScroll": 1,
    "arrows": true,
    "prevArrow": '<button type="button" class="slider__prev"></button>',
    "nextArrow": '<button type="button" class="slider__next"></button>',
  };

  constructor() { }

  ngOnInit(): void { }
}
